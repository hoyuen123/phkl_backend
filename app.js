// Copyright 2017 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const credentials = require('./auth/PHKL-OCR-5e28d4bec131.json')

const vision = require('@google-cloud/vision');
const client = new vision.ImageAnnotatorClient({
  credentials
});

const admin = require('firebase-admin');
admin.initializeApp({
  credential: admin.credential.cert(credentials)
});
let db = admin.firestore();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '10mb' }))

app.post('/api/requestOCRResult', (req, res) => {
  client
    .textDetection({
      image: { content: req.body.imageContent }
    }).then(response => {
      const ocrText = response[0].fullTextAnnotation.text
      const ocrResult = {
        ocrText: ocrText
      };
      storeOCRResult(ocrResult);
      res.status(200).send(ocrResult).end();
      // res.status(200).send(response).end();
    }).catch(err => {
      console.error(err);
      res.status(500).send(err).end();
    });
});

const storeOCRResult = (ocrResult) => {
  let timestamp = new Date().toISOString();
  
  let docRef = db.collection('ocrResult').doc(timestamp);
  docRef.set({
    ocrText: ocrResult.ocrText,
    timestamp: timestamp
  }).then(() => {
    console.log(timestamp, " => ocrResult created");
  }).catch(err => {
    console.error(err);
  });
};


// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

module.exports = app;
